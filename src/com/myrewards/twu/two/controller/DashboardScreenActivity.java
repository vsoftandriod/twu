package com.myrewards.twu.two.controller;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.twu.two.model.NoticeId;
import com.myrewards.twu.two.utils.DatabaseHelper;
import com.myrewards.twu.two.utils.Utility;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseInstallation;
import com.parse.PushService;
import com.readystatesoftware.viewbadger.BadgeView;

/**
 * 
 * @author HARI This class is used to display like menu
 */

public class DashboardScreenActivity extends Activity implements
		OnClickListener {

	// custom dialog fields
	public TextView logoutText, alertTitleTV;
	public Button loginbutton1;
	public Button cancelbutton1;
	final private static int LOGOUT = 1;
	public ImageView lalertImg;
	public TextView textLog;
	ImageView noticeboardBadge;
	DatabaseHelper helper;
	static int noticeCount = 0;
	public ImageView noticeboardunread;
	public static boolean noticeboardcount = false;

	ArrayList<NoticeId> noticeid;
	DatabaseHelper dbHelper;

	/** Called when the activity is first created. */

	boolean doubleBackToExitPressedOnce = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard1);

		getWindow().setSoftInputMode(
			    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
			);
		
		
		dbHelper = new DatabaseHelper(this);
		/*
		 * GrabItNowService.getGrabItNowService().sendNoticeBoardCountIdRequset(
		 * this);
		 */

		// Push Notifications Adding...................
		Parse.initialize(this, "45VXtf61vPWjvTUyQ3zYir0rUXkObz8iMb34qv6q",
				"51Zs9x1vBfenKEXzs6ZUvpCXPGAYj35zwgYtUXHU");

		PushService.setDefaultPushCallback(this,
				AppPushNotificationActivity.class);
		ParseInstallation.getCurrentInstallation().saveInBackground();
		ParseAnalytics.trackAppOpened(getIntent());

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerIVID);
		headerImage.getLayoutParams().height = Utility.screenHeight / 10;

		RelativeLayout myUnionRL = (RelativeLayout) findViewById(R.id.myUnionRLID);
		myUnionRL.setOnClickListener(this);
		RelativeLayout myDelegatesRL = (RelativeLayout) findViewById(R.id.myDelegatesRLID);
		myDelegatesRL.setOnClickListener(this);
		RelativeLayout myCardRL = (RelativeLayout) findViewById(R.id.myCardRLID);
		myCardRL.setOnClickListener(this);
		RelativeLayout myUnionContactsRL = (RelativeLayout) findViewById(R.id.myUnionContactsRLID);
		myUnionContactsRL.setOnClickListener(this);
		RelativeLayout myNoticeBoardRL = (RelativeLayout) findViewById(R.id.myNoticeBoardRLID);
		myNoticeBoardRL.setOnClickListener(this);
		RelativeLayout sendAFriendRL = (RelativeLayout) findViewById(R.id.sendAFriendRLID);
		sendAFriendRL.setOnClickListener(this);
		RelativeLayout mySpecialOffersRL = (RelativeLayout) findViewById(R.id.mySpecialOffersRLID);
		mySpecialOffersRL.setOnClickListener(this);
		RelativeLayout searchMyRewardsRL = (RelativeLayout) findViewById(R.id.searchMyRewardsRLID);
		searchMyRewardsRL.setOnClickListener(this);
		// set logout from cepu
		RelativeLayout lagoutRL = (RelativeLayout) findViewById(R.id.lagoutRLID);

		lagoutRL.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				showDialog(LOGOUT);
			}
		});

		RelativeLayout myParkingTimeRL = (RelativeLayout) findViewById(R.id.myParkingTimeRLID);
		myParkingTimeRL.setOnClickListener(this);
		RelativeLayout myissueRL = (RelativeLayout) findViewById(R.id.myissueRLID);
		myissueRL.setOnClickListener(this);
		RelativeLayout myAccountRL = (RelativeLayout) findViewById(R.id.myAccountRLID);
		myAccountRL.setOnClickListener(this);
		helper = new DatabaseHelper(DashboardScreenActivity.this);
		noticeboardunread = (ImageView) findViewById(R.id.noticeBadgeID);
		BadgeView badge = new BadgeView(DashboardScreenActivity.this,
				noticeboardunread);
		if (helper.getNoticeDetails() != 0) {
			badge.setText(Integer.toString(helper.getNoticeDetails()));
			badge.setBadgeBackgroundColor(Color.parseColor("#A8AE00"));
			badge.setBadgePosition(BadgeView.POSITION_BOTTOM_LEFT);
			badge.show();
		} else {
			badge.setVisibility(View.GONE);
			noticeboardunread.setVisibility(View.GONE);
		}
	}

	// to create custom alerts
	protected Dialog onCreateDialog(int id) {
		AlertDialog dialogDetails = null;
		LayoutInflater inflater;
		AlertDialog.Builder dialogbuilder;
		View dialogview;
		switch (id) {
		case LOGOUT:
			inflater = LayoutInflater.from(this);
			dialogview = inflater.inflate(R.layout.dialog_layout_logout, null);
			dialogbuilder = new AlertDialog.Builder(this);
			dialogbuilder.setCancelable(false);
			dialogbuilder.setView(dialogview);
			dialogDetails = dialogbuilder.create();

			break;
		case 2:
			inflater = LayoutInflater.from(this);
			dialogview = inflater.inflate(R.layout.dialog_layout_logout, null);
			dialogbuilder = new AlertDialog.Builder(this);
			dialogbuilder.setCancelable(false);
			dialogbuilder.setView(dialogview);
			dialogDetails = dialogbuilder.create();

			break;
		}
		return dialogDetails;
	}

	protected void onPrepareDialog(int id, Dialog dialog) {
		switch (id) {
		case LOGOUT:
			final AlertDialog alertDialog1 = (AlertDialog) dialog;
			alertTitleTV = (TextView) alertDialog1
					.findViewById(R.id.alertLogoutTitleTVID);
			alertTitleTV.setTypeface(Utility.font_bold);
			logoutText = (TextView) alertDialog1
					.findViewById(R.id.my_logoutTVID);
			logoutText.setTypeface(Utility.font_reg);
			loginbutton1 = (Button) alertDialog1
					.findViewById(R.id.loginBtnH_ID);
			loginbutton1.setTypeface(Utility.font_bold);
			cancelbutton1 = (Button) alertDialog1
					.findViewById(R.id.btn_cancelH_ID);
			cancelbutton1.setTypeface(Utility.font_bold);
			alertDialog1.setCancelable(false);
			loginbutton1.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					helper.deleteLoginDetails();

					Intent logoutIntent = new Intent(
							DashboardScreenActivity.this,
							LoginScreenActivity.class);
					logoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_SINGLE_TOP);
					startActivity(logoutIntent);
					LoginScreenActivity.etUserId.setText("");
					LoginScreenActivity.etPasswd.setText("");
					DashboardScreenActivity.this.finish();
					alertDialog1.dismiss();
				}
			});
			cancelbutton1.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					alertDialog1.dismiss();
				}
			});
			break;

		case 2:
			final AlertDialog alertDialog2 = (AlertDialog) dialog;
			alertTitleTV = (TextView) alertDialog2
					.findViewById(R.id.alertLogoutTitleTVID);
			alertTitleTV.setTypeface(Utility.font_bold);
			alertTitleTV.setText("Exit !");
			logoutText = (TextView) alertDialog2
					.findViewById(R.id.my_logoutTVID);
			logoutText.setTypeface(Utility.font_reg);
			logoutText.setText("Do you want to exit the Application ?");
			loginbutton1 = (Button) alertDialog2
					.findViewById(R.id.loginBtnH_ID);
			loginbutton1.setText("YES");
			loginbutton1.setTypeface(Utility.font_bold);
			cancelbutton1 = (Button) alertDialog2
					.findViewById(R.id.btn_cancelH_ID);
			cancelbutton1.setText("NO");
			cancelbutton1.setTypeface(Utility.font_bold);
			alertDialog2.setCancelable(false);
			loginbutton1.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					finish();
				}
			});
			cancelbutton1.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					alertDialog2.dismiss();
				}
			});
			break;
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.myUnionRLID:
			Intent intentMyUnion = new Intent(DashboardScreenActivity.this,
					MyUnionActivity.class);
			intentMyUnion.putExtra(Utility.DASHBOARD_ICON_ID, Utility.MY_UNION);
			startActivity(intentMyUnion);
			break;
		case R.id.myDelegatesRLID:
			Intent intentMyDelegates = new Intent(DashboardScreenActivity.this,
					MyDelegatesActivity.class);
			intentMyDelegates.putExtra(Utility.DASHBOARD_ICON_ID,
					Utility.MY_DELEGATES);
			startActivity(intentMyDelegates);
			break;
		case R.id.myCardRLID:
			Intent intentMyCard = new Intent(DashboardScreenActivity.this,
					MyCardActivity.class);
			intentMyCard.putExtra(Utility.DASHBOARD_ICON_ID, Utility.MY_CARD);
			startActivity(intentMyCard);
			break;
		case R.id.myUnionContactsRLID:
			Intent intentMyUnionContacts = new Intent(
					DashboardScreenActivity.this, MyUnionContactsActivity.class);
			intentMyUnionContacts.putExtra(Utility.DASHBOARD_ICON_ID,
					Utility.MY_UNION_CONTACTS);
			startActivity(intentMyUnionContacts);
			break;
		case R.id.myNoticeBoardRLID:
			Intent intentMyNoticeBoard = new Intent(
					DashboardScreenActivity.this, MyNoticeBoardActivity.class);
			intentMyNoticeBoard.putExtra(Utility.DASHBOARD_ICON_ID,
					Utility.MY_NOTICE_BOARD);
			startActivity(intentMyNoticeBoard);
			DashboardScreenActivity.this.finish();
			break;
		case R.id.sendAFriendRLID:
			Intent intentSendaFriend = new Intent(DashboardScreenActivity.this,
					SendAFriendNewActivity.class);
			intentSendaFriend.putExtra(Utility.DASHBOARD_ICON_ID,
					Utility.SEND_A_FRIEND);
			startActivity(intentSendaFriend);
			break;
		case R.id.mySpecialOffersRLID:
			Intent intentGIN1 = new Intent(DashboardScreenActivity.this,
					GrabitNowTabsActivity.class);
			intentGIN1.putExtra(Utility.DASHBOARD_ICON_ID, 1);
			startActivity(intentGIN1);
			break;
		case R.id.searchMyRewardsRLID:
			Intent intentGIN2 = new Intent(DashboardScreenActivity.this,
					GrabitNowTabsActivity.class);
			intentGIN2.putExtra(Utility.DASHBOARD_ICON_ID, 2);
			startActivity(intentGIN2);
			break;
		case R.id.myParkingTimeRLID:
			Intent intentMyParking = new Intent(DashboardScreenActivity.this,
					NewParkingTimeActivity.class);
			intentMyParking.putExtra(Utility.DASHBOARD_ICON_ID,
					Utility.MY_PARKING_TIME);
			startActivity(intentMyParking);
			break;
		case R.id.myissueRLID:
			Intent intentMyIssue = new Intent(DashboardScreenActivity.this,
					MyIssueActivity.class);
			intentMyIssue.putExtra(Utility.DASHBOARD_ICON_ID, Utility.MY_ISSUE);
			startActivity(intentMyIssue);
			break;
		case R.id.myAccountRLID:
			Intent intentMyAccount = new Intent(DashboardScreenActivity.this,
					MyAccountActivity.class);
			intentMyAccount.putExtra(Utility.DASHBOARD_ICON_ID,
					Utility.MY_ACCOUNT);
			startActivity(intentMyAccount);
			break;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			exitApplication();
			// showDialog(2);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	private void exitApplication() {

		if (doubleBackToExitPressedOnce) {

			DashboardScreenActivity.this.finish();

		} else {
			doubleBackToExitPressedOnce = true;
			Toast.makeText(DashboardScreenActivity.this,
					"Press again to minimize the app.", 100).show();
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					doubleBackToExitPressedOnce = false;
				}
			}, 2000);

		}

	}

	private boolean isGpsAvailable() {
		boolean haveGPS = false;
		try {
			PackageManager pm = this.getPackageManager();
			if (pm.hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS)) {
				haveGPS = true;
			} else {
				haveGPS = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.v("Hari--->", e.getMessage());
		}
		return haveGPS;
	}
}