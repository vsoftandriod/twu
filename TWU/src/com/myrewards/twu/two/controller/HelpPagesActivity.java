package com.myrewards.twu.two.controller;

import java.util.List;
import java.util.Vector;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.myrewards.twu.two.model.HelpPageFive;
import com.myrewards.twu.two.model.HelpPageFour;
import com.myrewards.twu.two.model.HelpPageOne;
import com.myrewards.twu.two.model.HelpPageSix;
import com.myrewards.twu.two.model.HelpPageThree;
import com.myrewards.twu.two.model.HelpPageTwo;


/**
 * The <code>ViewPagerFragmentActivity</code> class is the fragment activity hosting the ViewPager  
 * @author mwho
 */
public class HelpPagesActivity extends FragmentActivity{
	/** maintains the pager adapter*/
	private PagerAdapter mPagerAdapter;
	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.helpviewpager_layout);
		//initialsie the pager
		try {
			this.initialisePaging();
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->DEBUG", e);
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Initialise the fragments to be paged
	 */
	private void initialisePaging() {
		
		List<Fragment> fragments = new Vector<Fragment>();
		fragments.add(Fragment.instantiate(this, HelpPageOne.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageTwo.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageThree.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageFour.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageFive.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageSix.class.getName()));
		this.mPagerAdapter  = new PagerAdapter(super.getSupportFragmentManager(), fragments);
		//
		ViewPager pager = (ViewPager)super.findViewById(R.id.viewpager);
		pager.setAdapter(this.mPagerAdapter);
	}

}
